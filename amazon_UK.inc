<?php
/* $Id',amazon_US.inc,v 1.1 2007/01/02 01:54:06 prometheus6 Exp $ */
  global $_amazon_search_browse_fields;
  global $_amazon_search_browse_nodes;
  /**
    * $_amazon_search_browse_fields holds the field to search for each SearchIndex.
    * Valid combinations can be found at
    * http://www.amazon.com/gp/aws/sdk/main.html?s=AWSEcommerceService&v=4-0&p=ApiReference/UsSearchIndexMatrixArticle
  */
  $_amazon_search_browse_fields =
  array(
    'Books' => array('Author'=>'Author','Keywords'=>'Keywords','Title'=>'Title'),
    'Music' => array('Artist' => 'Artist','Keywords'=>'Keywords', 'Title' => 'Title',),
    'DVD' => array('Actor' => 'Actor','Keywords'=>'Keywords', 'Director' => 'Director', 'Title'=>'Title',),
  );

  $_amazon_search_browse_nodes =
  array(
		'Books' => array(
		  507848 => 'Address Books, Journals & More',
		  91 => 'Art, Architecture & Photography',
		  51 => 'Audio Cassettes',
		  267859 => 'Audio CDs',
		  67 => 'Biography',
		  68 => 'Business, Finance & Law',
		  69 => 'Children\'s Books',
		  274081 => 'Comics & Graphic Novels',
		  71 => 'Computers & Internet',
		  72 => 'Crime, Thrillers & Mystery',
		  637262 => 'e-Books',
		  62 => 'Fiction',
		  66 => 'Food & Drink',
		  275835 => 'Gay & Lesbian',
		  74 => 'Health, Family & Lifestyle',
		  65 => 'History',
		  64 => 'Home & Garden',
		  63 => 'Horror',
		  89 => 'Humour',
		  61 => 'Mind, Body & Spirit',
		  73 => 'Music, Stage & Screen',
		  275389 => 'Poetry, Drama & Criticism',
		  59 => 'Reference & Languages',
		  58 => 'Religion & Spirituality',
		  88 => 'Romance',
		  57 => 'Science & Nature',
		  56 => 'Science Fiction & Fantasy',
		  564334 => 'Scientific, Technical & Medical',
		  60 => 'Society, Politics & Philosophy',
		  55 => 'Sports, Hobbies & Games',
		  83 => 'Travel & Holiday',
		  52 => 'Young Adult',
		),
		'DVD' => array(
		  501778 => 'Action & Adventure',
		  501796 => 'Art House & International',
		  501858 => 'Children\'s DVD',
		  501976 => 'Classics',
		  501866 => 'Comedy',
		  501872 => 'Drama',
		  655852 => 'DVD Special Offers',
		  501880 => 'Horror & Suspense',
		  501888 => 'Music',
		  1108824 => 'Musicals & Classical',
		  501912 => 'Science Fiction & Fantasy',
		  501940 => 'Sports & Fitness',
		  501958 => 'Television & Documentary',
		),
		'Music' => array(
		  694228 => 'Adult Contemporary',
		  557264 => 'Blues',
		  499368 => 'Children\'s Music',
		  712822 => 'Christmas Music',
		  697386 => 'Classical',
		  547786 => 'Compilations',
		  231177 => 'Country',
		  231183 => 'Dance & Electronic',
		  231219 => 'Easy Listening',
		  690892 => 'Hard Rock & Metal',
		  754574 => 'Hip-Hop & Rap',
		  231193 => 'Indie',
		  231201 => 'Jazz',
		  231213 => 'Miscellaneous',
		  694208 => 'Pop',
		  754576 => 'R&B and Soul',
		  231239 => 'Rock',
		  231249 => 'Soundtracks',
		  231254 => 'World & Folk',
		)
	);