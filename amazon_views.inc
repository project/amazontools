<?php

function amazon_views_tables() {
  $tables['amazonnode'] = array(
    'name' => 'amazonnode',
    "provider" => "amazon",
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'rating' => array(
        'name' => t('Amazon: Product rating'),
        'sortable' => TRUE,
        'help' => t('The user rating of an amazon product.'),
      ),
    ),
    'sorts' => array(
      'rating' => array(
        'name' => t('Amazon: Product rating'),
        'help' => t('This allows you to sort by the user rating of an amazon product.'),
      ),
    ),
    'filters' => array(
      'asin' => array(
        'name' => t("Amazon: Has link"),
        'list' => array('NULL' => 'have Amazon item'),
        'list-type' => "select",
        'operator' => array('IS NOT' => "Does", 'IS' => "Does not"),
        'handler' => 'amazon_handler_has_asin',
        'help' => t("This will filter nodes that do or do not have Amazon links."),
      ),
    ),
  );

  $tables['amazonitem'] = array(
    'name' => 'amazonitem',
    "provider" => "amazon",
    'join' => array(
      'left' => array(
        'table' => 'amazonnode',
        'field' => 'asin'
      ),
      'right' => array(
        'field' => 'asin'
      ),
    ),
    'fields' => array(
      'asin' => array(
        'name' => t('Amazon: ASIN'),
        'addlfields' => array('detailpageurl'),
        'handler' => array(
           'amazon_handler_field_clickable' => 'Show as link',
           '' => 'Show as text',
        ),
      ),
      'title' => array(
        'name' => t('Amazon: Product title'),
        'sortable' => TRUE,
        'addlfields' => array('detailpageurl'),
        'handler' => array(
           'amazon_handler_field_clickable' => 'Show as link',
           '' => 'Show as text',
        ),
      ),
      'author' => array(
        'name' => t('Amazon: Author'),
        'handler' => 'amazon_handler_author',
        'sortable' => TRUE,
      ),
      'smallimageurl' => array(
        'name' => t('Amazon: Product Thumbnail (small)'),
        'addlfields' => array('title', 'detailpageurl'),
        'handler' => array(
           'amazon_handler_field_image' => 'Plain image',
           'amazon_handler_field_image_link_to_node' => 'Image linked to node',
           'amazon_handler_field_image_link_to_product' => 'Image linked to product',
        ),
      ),
      'mediumimageurl' => array(
        'name' => t('Amazon: Product Thumbnail (medium)'),
        'addlfields' => array('title', 'detailpageurl'),
        'handler' => array(
           'amazon_handler_field_image' => 'Plain image',
           'amazon_handler_field_image_link_to_node' => 'Image linked to node',
           'amazon_handler_field_image_link_to_product' => 'Image linked to product',
        ),
      ),
      'largeimageurl' => array(
        'name' => t('Amazon: Product Thumbnail (large)'),
        'addlfields' => array('title', 'detailpageurl'),
        'handler' => array(
           'amazon_handler_field_image' => 'Plain image',
           'amazon_handler_field_image_link_to_node' => 'Image linked to node',
           'amazon_handler_field_image_link_to_product' => 'Image linked to product',
        ),
      ),
      'binding' => array(
        'name' => t('Amazon: Binding'),
        'sortable' => TRUE,
      ),
      'formattedprice' => array(
        'name' => t('Amazon: Price'),
        'sortable' => TRUE,
      ),
      'availability' => array(
        'name' => t('Amazon: Availability'),
        'sortable' => TRUE,
      ),
    ),
  );

  return $tables;
}

function amazon_handler_author($fieldinfo, $fielddata, $value, $data) {
  $authors = unserialize($value);
  if (is_array($authors)) {
    return implode(', ', $authors);
  }
  else {
    return $authors;
  }
}

function amazon_handler_field_clickable($fieldinfo, $fielddata, $value, $data) {
  return l($value, $data->amazonitem_detailpageurl);
}

function amazon_handler_has_asin($op, $filter, $filterinfo, &$query) {
  $tn = $query->add_table($filterinfo['table']);
  $query->add_where("%s %s %s", $filter['field'], $filter['operator'], $filter['value']);
}

function amazon_handler_field_image($fieldinfo, $fielddata, $value, $data) {
  $title =  $data->amazonitem_title;
  $alt =  $data->amazonitem_title . ' ' . t('Cover image');
  $image = theme('image', $value, $alt, $title, $attributes, FALSE);
  return $image;
}

function amazon_handler_field_image_link_to_node($fieldinfo, $fielddata, $value, $data) {
  $title =  $data->amazonitem_title;
  $alt =  $data->amazonitem_title . ' ' . t('cover image');
  $image = theme('image', $value, $alt, $title, $attributes, FALSE);
  return l($image, 'node/' . $data->nid, array(), NULL, NULL, FALSE, TRUE);
}

function amazon_handler_field_image_link_to_product($fieldinfo, $fielddata, $value, $data) {
  $title =  $data->amazonitem_title;
  $alt =  $data->amazonitem_title . ' ' . t('cover image');
  $image = theme('image', $value, $alt, $title, $attributes, FALSE);
  return l($image, $data->amazonitem_detailpageurl, array(), NULL, NULL, FALSE, TRUE);
}